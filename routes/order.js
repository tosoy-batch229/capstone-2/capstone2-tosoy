const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

// Create order
router.post("/create", auth.verify, (req, res) => {
  const data = {
    user: auth.decode(req.headers.authorization),
    order: req.body,
  };
  orderControllers.createOrder(data).then((result) => res.send(result));
});

// Retrieve all orders - ADMIN
router.get("/allOrders", auth.verify, (req, res) => {
  const data = auth.decode(req.headers.authorization);
  orderControllers
    .getAllOrders(data)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
