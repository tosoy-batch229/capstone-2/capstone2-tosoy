const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// Create a product
router.post("/create", auth.verify, (req, res) => {
    const data = auth.decode(req.headers.authorization).isAdmin;
    productControllers
        .addProduct(data, req.body)
        .then((resultFromController) => res.send(resultFromController));
});

// Get all products
router.get("/all", (req, res) => {
    const data = auth.decode(req.headers.authorization);
    productControllers
        .getAllProducts(data)
        .then((resultFromController) => res.send(resultFromController));
});

// Get all active products
router.get("/active", (req, res) => {
    productControllers
        .getActiveProducts()
        .then((resultFromController) => res.send(resultFromController));
});

// Get a specific products
router.get("/specific", (req, res) => {
    const id = req.body;
    productControllers
        .getProduct(id)
        .then((resultFromController) => res.send(resultFromController));
});

// Update a product [ ADMIN ]
router.put('/update', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    const productID = req.body._id;
    productControllers
        .updateProduct(user, productID, req.body)
        .then((resultFromController) => res.send(resultFromController));
});

// Archive a product [ ADMIN ]
router.put('/archive', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    const productID = req.body._id;
    productControllers
        .archiveProduct(user, productID, req.body)
        .then(resultFromController => res.send(resultFromController))
});

module.exports = router;
