const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js');
const auth = require('../auth.js');


// Test: Get All User [ ADMIN ]
router.get('/testAll/', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    userControllers
        .getAllUsersAdmin(user)
        .then(resultFromController => res.send(resultFromController));
    }
);

// Get User Details
router.get('/specific', auth.verify, (req, res) =>{
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        user  : req.body
    };
    userControllers
        .getUser(data)
        .then(resultFromController => res.send(resultFromController));
    }
);

// Register New User Route
router.post('/register', (req, res) => {
    userControllers
        .registerNewUser(req.body)
        .then(resultFromController => res.send(resultFromController));
    }
);

// User Authentication Route
router.post('/userAuth', (req, res) => {
    userControllers
        .authenticateUser(req.body)
        .then(resultFromController => res.send(resultFromController)
    );
});

// ----------------- [ STRECTH GOALS ] -------------------- //
// Set user admin
router.put('/set', auth.verify, (req, res) => {
    const data = {
        admin : auth.decode(req.headers.authorization).isAdmin,
        userID : req.body.id
    };
    userControllers
        .setUserAsAdmin(data)
        .then(resultFromController => res.send(resultFromController));
});

// Get authenticated user's orders
router.get('/authUserOrders', auth.verify, (req, res) => {
    const data = auth.decode(req.headers.authorization);
    userControllers
        .getAuthUserOrder(data)
        .then(resultFromController => res.send(resultFromController));
})

module.exports = router;