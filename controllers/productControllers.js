const Products = require("../models/Products.js");

// Create a product - ADMIN
module.exports.addProduct = async (user, reqBody) => {
  const { name, description, price, author } = reqBody;
  // Check if the user is admin.
  if (user) {
    // Check if the product already exists.
    const existingProduct = await Products.findOne({ name: name });
    if (existingProduct) {
      return { error: "Product already exists!" };
    }
    // Create a new product.
    let newProduct = new Products({ name, description, price, author });
    await newProduct.save();
    return { Message: "New product created successfully" };
  } else {
    return { message: "You are not an admin!" };
  }
};

// Get all products - ADMIN
module.exports.getAllProducts = async (data) => {
  const result = await Products.find({});
  if (data?.isAdmin) {
    return result.length > 0
      ? result
      : { message: "No products is currently on display" };
  } else {
    return { message: "You are not an admin!" };
  }
};

// Get all active products
module.exports.getActiveProducts = async () => {
  const result = await Products.find({ isActive: true });
  return result.length > 0 ? result : { message: "No active products" };
};

// Get a single product
module.exports.getProduct = async (reqBody) => {
  const result = await Products.findById(reqBody._id);
  return result != null ? result : { message: "Product Not Found" };
};

// Update a product - ADMIN
module.exports.updateProduct = async (user, productID, reqBody) => {
  const { name, author, description, price, isActive } = reqBody;
  const productInDB = await Products.findById(productID);

  if (user.isAdmin) {
    // Check if the product is existing.
    if (!productInDB) {
      return { message: "Product not found!" };
    }
    // Check if the product is already up-to-date.
    if (
      Object.is(productInDB.name, name) &&
      Object.is(productInDB.author, author) &&
      Object.is(productInDB.description, description) &&
      Object.is(productInDB.price, price) &&
      Object.is(productInDB.isActive, isActive)
    ) {
      return { message: "Product is up-to-date " };
    }
    // If all is good
    await Products.findByIdAndUpdate(productID, {
      $set: { name, description, price, isActive, author },
    });
    return { message: "Updated Successfully!" };
  } else {
    return { message: "You are not an admin!" };
  }
};

// Deactivate / Activate a product - ADMIN
module.exports.archiveProduct = async (user, productID, reqBody) => {
  if (user.isAdmin) {
    let archive = { isActive: reqBody.isActive };
    await Products.findByIdAndUpdate(productID, archive);
    return { message: "Successfully!" };
  } else {
    return { message: "Something Went Wrong!" };
  }
};
