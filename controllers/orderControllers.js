const Orders = require("../models/Orders.js");
const Products = require("../models/Products.js");

// Create order
module.exports.createOrder = async (data) => {
  const { user, order } = data;

  const product = await Products.findById(order._id);
  if (product) {
    let totalAmount = product.price * order.quantity;

    let newOrder = new Orders({
      userId: user.id,
      products: [
        {
          productId: product._id,
          productName: product.name,
          quantity: order.quantity,
        },
      ],
      totalAmount,
      purchasedOnDate: new Date(),
    });

    await newOrder.save();
    return { message: "Thank You For Buying" };
  } else {
    add;
    return { message: "Product not found!" };
  }
};

// Get all orders
module.exports.getAllOrders = async (data) => {
  let orders = await Orders.find(
    { userId: data.id },
    "-_id -__v -products._id"
  );

  return orders.length > 0 ? orders : { message: "No orders" };
};
