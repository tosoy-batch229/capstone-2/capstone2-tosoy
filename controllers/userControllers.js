const Users = require("../models/Users.js");
const Orders = require("../models/Orders.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Get All User [ ADMIN ]
module.exports.getAllUsersAdmin = async (user) => {
  if (!user.isAdmin) {
    return { error: "You are not an admin!" };
  }
  const users = await Users.find({}, "_id -password");
  return users;
};

// Get a User
module.exports.getUser = async (data) => {
  const { isAdmin, user } = data;
  if (!isAdmin) {
    return { error: "You are not an admin!" };
  }
  const userDetails = await Users.findById(user.id, "-password");
  return userDetails != null ? userDetails : { error: "User not found!" };
};

// Register New User.
module.exports.registerNewUser = async (reqBody) => {
  const { username, email, password, isAdmin } = reqBody;
  const existingUser = await Users.findOne({ email: email });
  if (existingUser) {
    return { Error: "User already exists!" };
  } else {
    let newUser = new Users({
      username: username,
      email: email,
      password: bcrypt?.hashSync(password, 10),
      isAdmin: isAdmin,
    });
    await newUser.save();
    return { Message: "User created successfully" };
  }
};

// User Authentication
module.exports.authenticateUser = (reqBody) => {
  return Users.findOne({ username: reqBody.username }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      return isPasswordCorrect
        ? { access: auth.createAccessTokens(result) }
        : false;
    }
  });
};

// [ STRETCH GOALS ]
// Set User as admin
module.exports.setUserAsAdmin = async (data) => {
  const { admin, userID } = data;
  const result = await Users.findById(userID);
  if (!admin) {
    return { error: `You're not an admin` };
  }
  if (result == null) {
    return { error: "User not found!" };
  }
  if (!result.isAdmin) {
    await Users.findByIdAndUpdate(result, { $set: { isAdmin: true } });
    return { message: "Completed Successfully!" };
  } else {
    return { error: "User is an admin already!" };
  }
};

// Get authenticated user's orders
module.exports.getAuthUserOrder = async (data) => {
  if (!data) {
    return { error: "User must logged in" };
  }
  if (data.isAdmin) {
    return { error: "Non-admin users only!" };
  }
  const result = await Orders.find(
    { userId: data.id },
    "products.productName products.quantity -_id totalAmount"
  );

  if (result.length > 0) {
    return result;
  } else {
    return { error: "No order forund for this user." };
  }
};
